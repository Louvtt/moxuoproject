#include <mxengine/core/application.h>

class App : public mx::application {
public:
    App() : mx::application("App") {}
};

mx::application* mx::createApplication(int argc, char* argv[]) {
    return new App();
}